package Translation;

import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.language_translator.v3.LanguageTranslator;
import com.ibm.watson.language_translator.v3.model.TranslateOptions;
import com.ibm.watson.language_translator.v3.model.Translation;
import com.ibm.watson.language_translator.v3.model.TranslationResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * TranslatorController class handling translation from IBM Watson
 */
public class TranslatorController {
    private final String API_KEY; //API_KEY to IMB Watson Language Translator
    private final String URL; //URL where to send the request

    private Authenticator authenticator; // Authenticator from ibm.cloud.sdk
    private LanguageTranslator languageTranslator; //Service for language translator

    private String textToTranslate; //Text to be translated, inserted by user
    private String translatedText; //Translated text, shown to user
    private String langFrom = "cs"; //Language translated from
    private String langTo = "en"; //Language translated to

    private final Map<String, String> LANG_MAP = new HashMap<String, String>(); //Map of language code and its name

    /**
     * Translate controller constructor,
     * initializing Language Translator and populating LANG_MAP.
     *
     * @param API_KEY api for the Language Translator service from IBM WATSON
     * @param URL     url of the Language Translator service from IBM WATSON
     */
    public TranslatorController(String API_KEY, String URL) {
        this.API_KEY = API_KEY;
        this.URL = URL;
        initializeLanguageTranslator();
        populateLANG_MAP();
    }

    /**
     * Initializing Language Translator service class,
     * which handles sending and receiving of the translation.
     */
    private void initializeLanguageTranslator() {
        authenticator = new IamAuthenticator(API_KEY);
        languageTranslator = new LanguageTranslator("2018-05-01", authenticator);
        languageTranslator.setServiceUrl(URL);
    }

    /**
     * Sending text to Language Translator from IBM watson
     * and receiving translated text, saves translated text
     * to class variable translatedText.
     */
    public void getTranslation() {
        translatedText = "";
        StringBuilder stringBuilder = new StringBuilder();

        TranslateOptions translateOptions =
                new TranslateOptions.Builder().addText(textToTranslate).modelId(getModelId()).build();
        TranslationResult translationResult = languageTranslator.translate(translateOptions).execute().getResult();

        List<Translation> translationList = translationResult.getTranslations();

        for (Translation translation : translationList) {
            stringBuilder.append(translation.getTranslation());
        }

        translatedText = stringBuilder.toString();
    }

    public String getTextToTranslate() {
        return textToTranslate;
    }

    public void setTextToTranslate(String textToTranslate) {
        this.textToTranslate = textToTranslate;
    }

    public String getTranslatedText() {
        return translatedText;
    }

    public void setTranslatedText(String translatedText) {
        this.translatedText = translatedText;
    }

    /**
     * Getting modelID for translation,
     * it means from what language to what language the transaction is going to be
     *
     * @return modelID
     */
    public String getModelId() {
        return langFrom + "-" + langTo;
    }

    /**
     * Populating LANG_MAP with language codes and names
     */
    private void populateLANG_MAP() {
        LANG_MAP.put("Čeština", "cs");
        LANG_MAP.put("Němčina", "de");
        LANG_MAP.put("Dánština", "da");
        LANG_MAP.put("Řečtina", "el");
        LANG_MAP.put("Italština", "it");
        LANG_MAP.put("Polština", "pl");
        LANG_MAP.put("Portugalština", "pt");
        LANG_MAP.put("Slovenština", "sk");
    }

    /**
     * Setting class variable langTo,
     * it is the language to which the translation is made
     *
     * @param lang to be set as langTo
     */
    public void setLangFrom(String lang) {
        langFrom = LANG_MAP.get(lang);
    }
}
