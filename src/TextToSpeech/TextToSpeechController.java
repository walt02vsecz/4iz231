package TextToSpeech;

import Translation.TranslatorController;
import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.text_to_speech.v1.TextToSpeech;
import com.ibm.watson.text_to_speech.v1.model.SynthesizeOptions;
import com.ibm.watson.text_to_speech.v1.util.WaveUtils;

import javax.sound.sampled.*;
import java.io.*;


/**
 * TextToSpeechController class is handling conversion
 * of translated text to audio and playing it to user
 */
public class TextToSpeechController {
    private final String API_KEY; //API_KEY to IMB Watson Language Translator
    private final String URL; //URL where to send the request

    private TranslatorController translatorController;

    private TextToSpeech synthesizer;
    private Authenticator authenticator;

    boolean playCompleted; // indications of whether the playback is completed or not
    private String selectedVoice = SynthesizeOptions.Voice.EN_US_LISAVOICE;

    private Clip audioClip;
    private AudioInputStream audioInputStream;
    private File audioFile = new File("output.wav");

    /**
     * TextToSpeechController constructor
     *
     * @param API_KEY              to the textToSpeech service
     * @param URL                  to the textToSpeech service
     * @param translatorController class
     */
    public TextToSpeechController(String API_KEY, String URL, TranslatorController translatorController) {
        this.API_KEY = API_KEY;
        this.URL = URL;
        this.translatorController = translatorController;
        initializeTextToSpeech();
    }

    /**
     * Initializing IBM Watson textToSpeech service
     */
    private void initializeTextToSpeech() {
        authenticator = new IamAuthenticator(API_KEY);
        synthesizer = new TextToSpeech(authenticator);
        synthesizer.setServiceUrl(URL);
    }

    /**
     * Getting audio from IMB Watson - textToSpeech service
     * and writing it to a file
     */
    public void readTranslation() {
        SynthesizeOptions synthesizeOptions =
                new SynthesizeOptions.Builder()
                        .text(translatorController.getTranslatedText())
                        .voice(selectedVoice)
                        .accept("audio/wav")
                        .build();
        InputStream in = synthesizer.synthesize(synthesizeOptions).execute().getResult();
        try {
            writeToFile(WaveUtils.reWriteWaveHeader(in), audioFile);
        } catch (Exception e) {
            //do nothing
        }

        playAudioFile(audioFile);
    }

    /**
     * Write the input stream to a file.
     *
     * @param in   audio stream to input in file
     * @param file where to input the audio stream
     */
    private static void writeToFile(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets selectedVoice according to the tone of the text
     *
     * @param tone on which is voice selected
     */
    public void setSelectedVoice(String tone) {
        if (tone.isEmpty()) {
            selectedVoice = SynthesizeOptions.Voice.EN_US_LISAVOICE;
        }
        switch (tone) {
            case "anger":
                selectedVoice = SynthesizeOptions.Voice.EN_GB_KATEV3VOICE;
                break;
            case "fear":
                selectedVoice = SynthesizeOptions.Voice.EN_US_HENRYV3VOICE;
                break;
            case "joy":
                selectedVoice = SynthesizeOptions.Voice.EN_US_KEVINV3VOICE;
                break;
            case "sadness ":
                selectedVoice = SynthesizeOptions.Voice.EN_US_ALLISONV3VOICE;
                break;
            case "analytical":
                selectedVoice = SynthesizeOptions.Voice.EN_GB_KATEVOICE;
                break;
            case "confident":
                selectedVoice = SynthesizeOptions.Voice.EN_US_OLIVIAV3VOICE;
                break;
            case "tentative ":
                selectedVoice = SynthesizeOptions.Voice.EN_US_MICHAELV3VOICE;
                break;
        }
    }

    /**
     * Playing the sound of the audio file from IBM Watson
     *
     * @param audioFile to be played
     */
    private void playAudioFile(File audioFile) {
        try {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
            AudioFormat format = audioStream.getFormat();
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
            SourceDataLine audioLine = (SourceDataLine) AudioSystem.getLine(info);

            audioLine.open(format);
            audioLine.start();

            int BUFFER_SIZE = 4096;
            byte[] bytesBuffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;

            while ((bytesRead = audioStream.read(bytesBuffer)) != -1) {
                audioLine.write(bytesBuffer, 0, bytesRead);
            }

            audioLine.drain();
            audioLine.close();
            audioStream.close();

        } catch (UnsupportedAudioFileException ex) {
            System.out.println("The specified audio file is not supported.");
            ex.printStackTrace();
        } catch (LineUnavailableException ex) {
            System.out.println("Audio line for playing back is unavailable.");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Error playing the audio file.");
            ex.printStackTrace();
        }
    }
}
