package UI;

import TextToSpeech.TextToSpeechController;
import ToneAnalyzer.ToneAnalyzerController;
import Translation.TranslatorController;

public class UI {
    MainFrame mainFrame;

    /**
     * UI constructor, initializing UI
     *
     * @param translatorController   class
     * @param toneAnalyzerController class
     * @param textToSpeechController class
     */
    public UI(TranslatorController translatorController, ToneAnalyzerController toneAnalyzerController, TextToSpeechController textToSpeechController) {
        mainFrame = new MainFrame(translatorController, toneAnalyzerController, textToSpeechController);
    }
}
