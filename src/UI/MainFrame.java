package UI;

import TextToSpeech.TextToSpeechController;
import ToneAnalyzer.ToneAnalyzerController;
import Translation.TranslatorController;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * MainFrame class, it is the main frame of the application,
 * which includes every other element of the application
 */
public class MainFrame extends JFrame {
    private JTextArea originalText;
    private JTextArea translateText;
    private JButton read;
    private JButton translate;
    private JPanel mainPanel;
    private JPanel buttonPanel;
    private JSplitPane splitPane;
    private JLabel translateLabel;
    private JLabel originalLabel;
    private JScrollPane translateScroll;
    private JScrollPane originalScroll;
    private JLabel readInfo;
    private JComboBox<String> fromLang;
    private JLabel fromLangLabel;
    private JLabel textTone;
    private JLabel textToneLabel;
    private JOptionPane emptyOriginalTextWarning;
    private JDialog emptyOriginalTextWarningDialog;
    final DefaultComboBoxModel<String> toLangModel = new DefaultComboBoxModel<>();

    /**
     * Controller classes
     */
    private TranslatorController translatorController;
    private ToneAnalyzerController toneAnalyzerController;
    private TextToSpeechController textToSpeechController;

    /**
     * Construction method for main frame of the application
     * initializing all properties of the main frame
     *
     * @param translatorController   class
     * @param toneAnalyzerController class
     * @param textToSpeechController class
     */
    public MainFrame(TranslatorController translatorController, ToneAnalyzerController toneAnalyzerController, TextToSpeechController textToSpeechController) {
        super("Translator 9000X");
        $$$setupUI$$$();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setContentPane(mainPanel);
        this.setSize(1280, 720);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        this.initOptionPane();
        this.initTranslateAction();
        this.initReadAction();
        this.initSelectLangAction();
        this.createModelsForComboBox();

        this.translatorController = translatorController;
        this.toneAnalyzerController = toneAnalyzerController;
        this.textToSpeechController = textToSpeechController;
    }

    /**
     * Initializing JOptionPane
     */
    private void initOptionPane() {
        emptyOriginalTextWarning = new JOptionPane("Text k překladu nesmí být prázdný!", JOptionPane.WARNING_MESSAGE);
        emptyOriginalTextWarningDialog = emptyOriginalTextWarning.createDialog("Warning!");
        emptyOriginalTextWarningDialog.setAlwaysOnTop(true);
        emptyOriginalTextWarningDialog.setVisible(false);
    }

    /**
     * Initializing actionListeners on Jbutton for translate.
     */
    private void initTranslateAction() {
        translate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String textForTranslation = originalText.getText();
                if (!textForTranslation.isEmpty()) {
                    translatorController.setTextToTranslate(textForTranslation);
                    translatorController.getTranslation();
                    translateText.setText(translatorController.getTranslatedText());
                    toneAnalyzerController.analyzeToneOfText(translatorController.getTranslatedText());
                    textTone.setText(toneAnalyzerController.getTextToneName());
                    textToSpeechController.setSelectedVoice(toneAnalyzerController.getTextToneId());
                    read.setEnabled(true);
                } else {
                    emptyOriginalTextWarningDialog.setVisible(true);
                    translateText.setText("");
                    read.setEnabled(false);
                    textTone.setText("");
                }
            }
        });
    }

    /**
     * Initializing actionListener on Jbutton for read
     */
    private void initReadAction() {
        read.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textToSpeechController.readTranslation();
            }
        });
    }

    /**
     * Initializing propertyChangeListeners on JComboBoxes with langFrom and langTo properties
     */
    private void initSelectLangAction() {
        fromLang.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                translatorController.setLangFrom(e.getItem().toString());
            }
        });
    }

    /**
     * Creating a populating models for ComboBoxes with selection of the from and to languages
     */
    private void createModelsForComboBox() {
        toLangModel.addElement("Čeština");
        toLangModel.addElement("Němčina");
        toLangModel.addElement("Čínština");
        toLangModel.addElement("Dánština");
        toLangModel.addElement("Bulharština");
        toLangModel.addElement("Řečtina");
        toLangModel.addElement("Italština");
        toLangModel.addElement("Polština");
        toLangModel.addElement("Portugalština");
        toLangModel.addElement("Malajština");
        toLangModel.addElement("Slovenština");
        toLangModel.addElement("Turečtina");
        fromLang.setModel(toLangModel);
    }


    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        Font mainPanelFont = this.$$$getFont$$$("JetBrains Mono", -1, -1, mainPanel.getFont());
        if (mainPanelFont != null) mainPanel.setFont(mainPanelFont);
        mainPanel.setForeground(new Color(-10724260));
        mainPanel.setMinimumSize(new Dimension(1280, 720));
        mainPanel.setPreferredSize(new Dimension(1280, 720));
        mainPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(-16777216)), null));
        originalScroll = new JScrollPane();
        originalScroll.setHorizontalScrollBarPolicy(31);
        originalScroll.setVerticalScrollBarPolicy(22);
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 3;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        mainPanel.add(originalScroll, gbc);
        originalText = new JTextArea();
        Font originalTextFont = this.$$$getFont$$$("JetBrains Mono", -1, 16, originalText.getFont());
        if (originalTextFont != null) originalText.setFont(originalTextFont);
        originalText.setLineWrap(true);
        originalText.setWrapStyleWord(true);
        originalScroll.setViewportView(originalText);
        translateScroll = new JScrollPane();
        translateScroll.setHorizontalScrollBarPolicy(31);
        translateScroll.setVerticalScrollBarPolicy(22);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 3;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        mainPanel.add(translateScroll, gbc);
        translateText = new JTextArea();
        translateText.setBackground(new Color(-1973538));
        translateText.setCaretColor(new Color(-3552823));
        translateText.setEditable(false);
        translateText.setEnabled(true);
        Font translateTextFont = this.$$$getFont$$$("JetBrains Mono", -1, 16, translateText.getFont());
        if (translateTextFont != null) translateText.setFont(translateTextFont);
        translateText.setLineWrap(true);
        translateText.setWrapStyleWord(true);
        translateScroll.setViewportView(translateText);
        translateLabel = new JLabel();
        Font translateLabelFont = this.$$$getFont$$$("JetBrains Mono", -1, 14, translateLabel.getFont());
        if (translateLabelFont != null) translateLabel.setFont(translateLabelFont);
        translateLabel.setText("Překlad do angličtiny:");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 3;
        gbc.weightx = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        mainPanel.add(translateLabel, gbc);
        originalLabel = new JLabel();
        Font originalLabelFont = this.$$$getFont$$$("JetBrains Mono", -1, 14, originalLabel.getFont());
        if (originalLabelFont != null) originalLabel.setFont(originalLabelFont);
        originalLabel.setText("Text k překladu ve vybraném jazyce:");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 3;
        gbc.weightx = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        mainPanel.add(originalLabel, gbc);
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayoutManager(2, 5, new Insets(0, 0, 0, 0), -1, -1));
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        mainPanel.add(buttonPanel, gbc);
        splitPane = new JSplitPane();
        splitPane.setContinuousLayout(false);
        splitPane.setDividerLocation(225);
        splitPane.setDividerSize(1);
        splitPane.setEnabled(false);
        buttonPanel.add(splitPane, new GridConstraints(0, 4, 2, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, new Dimension(450, -1), null, null, 0, false));
        read = new JButton();
        read.setEnabled(false);
        Font readFont = this.$$$getFont$$$("JetBrains Mono", -1, 16, read.getFont());
        if (readFont != null) read.setFont(readFont);
        read.setText("Přečíst");
        splitPane.setRightComponent(read);
        translate = new JButton();
        Font translateFont = this.$$$getFont$$$("JetBrains Mono", -1, 16, translate.getFont());
        if (translateFont != null) translate.setFont(translateFont);
        translate.setText("Přeložit");
        splitPane.setLeftComponent(translate);
        fromLang = new JComboBox();
        buttonPanel.add(fromLang, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(170, -1), null, 0, false));
        fromLangLabel = new JLabel();
        Font fromLangLabelFont = this.$$$getFont$$$("JetBrains Mono", -1, 16, fromLangLabel.getFont());
        if (fromLangLabelFont != null) fromLangLabel.setFont(fromLangLabelFont);
        fromLangLabel.setText("Z jazyka");
        buttonPanel.add(fromLangLabel, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        textToneLabel = new JLabel();
        Font textToneLabelFont = this.$$$getFont$$$("JetBrains Mono", -1, 16, textToneLabel.getFont());
        if (textToneLabelFont != null) textToneLabel.setFont(textToneLabelFont);
        textToneLabel.setText("Tón textu: ");
        buttonPanel.add(textToneLabel, new GridConstraints(1, 2, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        textTone = new JLabel();
        Font textToneFont = this.$$$getFont$$$("JetBrains Mono", -1, 16, textTone.getFont());
        if (textToneFont != null) textTone.setFont(textToneFont);
        textTone.setText("");
        buttonPanel.add(textTone, new GridConstraints(1, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        readInfo = new JLabel();
        readInfo.setText("Přečíst lze pouze přeložený text.");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        mainPanel.add(readInfo, gbc);
    }

    /**
     * @param fontName    fontName
     * @param style       style
     * @param size        size
     * @param currentFont active font
     * @return font
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @return mainPanel
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }

}
