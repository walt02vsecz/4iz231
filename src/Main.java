import TextToSpeech.TextToSpeechController;
import ToneAnalyzer.ToneAnalyzerController;
import Translation.TranslatorController;
import UI.UI;

/**
 * Main Class
 */
public class Main {
    /**
     * Main method, initializing all classes
     *
     * @param args main method args
     */
    public static void main(String[] args) {
        TranslatorController translatorController = new TranslatorController(
                "7JAYZHs4UBpGG3v3hwgjM63cYQURlhl-WFu2ivrrLNaq",
                "https://api.eu-gb.language-translator.watson.cloud.ibm.com/instances/a1bd7856-077c-4d6a-af7f-cfa48c9a3cf5");
        ToneAnalyzerController toneAnalyzerController = new ToneAnalyzerController(
                "sk7szyyvlikPK6IFBHOpnnF3_lidkuA8hM6Wk29ATn6F",
                "https://api.eu-gb.tone-analyzer.watson.cloud.ibm.com/instances/67488598-8873-4ea8-bca5-03c25455b41f");
        TextToSpeechController textToSpeechController = new TextToSpeechController(
                "oAsrxFNS1sWDIrYdqohJ4H96Tfn-ZcVzhxSEuFKjsWkq",
                "https://api.eu-gb.text-to-speech.watson.cloud.ibm.com/instances/69f8784b-1848-40ce-b92c-3ee57b3f8c9c",
                translatorController);
        UI ui = new UI(translatorController, toneAnalyzerController, textToSpeechController);
    }
}
