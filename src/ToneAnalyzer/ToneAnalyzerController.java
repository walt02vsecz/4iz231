package ToneAnalyzer;

import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.tone_analyzer.v3.ToneAnalyzer;
import com.ibm.watson.tone_analyzer.v3.model.ToneAnalysis;
import com.ibm.watson.tone_analyzer.v3.model.ToneOptions;
import com.ibm.watson.tone_analyzer.v3.model.ToneScore;

import java.util.List;

/**
 * ToneAnalyzerController class handling tone analyzing
 * for translated text from IBM Watson
 */
public class ToneAnalyzerController {
    private final String API_KEY; //API_KEY to IMB Watson Language Translator
    private final String URL; //URL where to send the request

    private Authenticator authenticator; // Authenticator from ibm.cloud.sdk
    private ToneAnalyzer toneAnalyzer; //Service for language translator

    private String textToneId; // Tone id of the text for translation
    private String textToneName; // Tone name of the text for translation

    /**
     * ToneAnalyzerController constructor
     *
     * @param API_KEY for IMB Watson tone analyzer service
     * @param URL     for IMB Watson tone analyzer service
     */
    public ToneAnalyzerController(String API_KEY, String URL) {
        this.API_KEY = API_KEY;
        this.URL = URL;
        initializeToneAnalyzer();
    }

    /**
     * Initializing IBM Watson authenticator and tone analyzer
     */
    private void initializeToneAnalyzer() {
        authenticator = new IamAuthenticator(API_KEY);
        toneAnalyzer = new ToneAnalyzer("2017-09-21", authenticator);
        toneAnalyzer.setServiceUrl(URL);
    }

    /**
     * Calling the IBM Watson tone analyzer service
     * and getting the text tone information
     *
     * @param text to be analyzed
     */
    public void analyzeToneOfText(String text) {
        ToneOptions toneOptions = new ToneOptions.Builder().text(text).build();
        ToneAnalysis tone = toneAnalyzer.tone(toneOptions).execute().getResult();
        List<ToneScore> toneScores = tone.getDocumentTone().getTones();

        getHighestTone(toneScores);
    }

    /**
     * Returns the tone with highest score
     */
    private void getHighestTone(List<ToneScore> toneScores) {
        double highestScore = 0;
        String highestToneId = "";
        String highestToneName = "";

        for (ToneScore toneScore : toneScores) {
            if (toneScore.getScore() > highestScore) {
                highestScore = toneScore.getScore();
                highestToneId = toneScore.getToneId();
                highestToneName = toneScore.getToneName();
            }
        }

        textToneId = highestToneId;
        textToneName = highestToneName;

        System.out.println("HighestScore: " + highestScore + ", HighestToneId: " + highestToneId + ", HighestToneName: " + highestToneName);
    }

    public String getTextToneId() {
        return textToneId;
    }

    public String getTextToneName() {
        if (textToneName.isEmpty()) {
            return "Žádný tón nebyl rozpoznán!";
        } else {
            return textToneName;
        }
    }
}
